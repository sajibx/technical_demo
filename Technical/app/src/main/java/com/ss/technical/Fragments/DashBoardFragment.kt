package com.ss.technical.Fragments

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.kittinunf.fuel.httpGet
import com.ss.technical.Activity.ProfileActivity
import com.ss.technical.MainActivity
import com.ss.technical.Model.Data
import com.ss.technical.Model.Search
import com.ss.technical.R
import com.ss.technical.UI.SearchAdapter
import kotlinx.coroutines.launch
import java.lang.Exception


class DashBoardFragment : BaseFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dash_board, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
    }

    fun init()
    {

        var btn_search = requireView().findViewById<TextView>(R.id.btn_search)
        var input_search = requireView().findViewById<EditText>(R.id.search_input)

        var profile = requireView().findViewById<TextView>(R.id.viewProfile)

        var label = requireView().findViewById<TextView>(R.id.label)

        var logout = requireView().findViewById<TextView>(R.id.logout)

        btn_search.setOnClickListener {

            if (input_search.text.toString().isEmpty())
            {
                Toast.makeText(context, "Input empty", Toast.LENGTH_LONG).show()
            }
            else
            {
                launch {

                    // api call

                    "http://api.tvmaze.com/search/shows?q=${input_search.text}"
                        .httpGet()
                        .header("Content-Type" to "application/json")
                        .responseObject(Search.Deserializer())
                        { request, response, result ->

                            when (result) {
                                is com.github.kittinunf.result.Result.Failure -> {

                                    val ex = result.getException()
                                    Log.e("sajib", ex.message!!)
                                    label.text = "No Search Result Found"

                                }
                                is com.github.kittinunf.result.Result.Success -> {

                                    Log.e("sajib", result.value.size.toString())

                                    if (result.value.size>0)
                                    {
                                        label.text = ""
                                    }
                                    else
                                    {
                                        label.text = "No Search Result"
                                    }

                                    // caching user list data
                                    if (!Data.search.isEmpty()) {
                                        Data.search.clear()
                                    }
                                    Data.search = result.value

                                    // updating ui with api data

                                    updateUI {

                                        try{

                                            // list genarating

                                            val recycler = requireView().findViewById<RecyclerView>(R.id.recycler)
                                            recycler.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false) as RecyclerView.LayoutManager?

                                            val adapter = SearchAdapter(Data.search)

                                            recycler.setHasFixedSize(true);
                                            recycler.adapter = adapter

                                        }catch (e: Exception)
                                        {
                                            ;
                                        }

                                    }


                                }


                            }
                        }

                }
            }

        }

        profile.setOnClickListener {
            startActivity(Intent(context, ProfileActivity::class.java))
        }

        logout.setOnClickListener {

            val pref = PreferenceManager.getDefaultSharedPreferences(context)
            val editor = pref.edit()
            editor
                .remove("username")
                .remove("password")
                .apply()

            requireActivity().finish()
            startActivity(Intent(context, MainActivity::class.java))

        }

    }

}