package com.ss.technical.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.ss.technical.Model.Data
import com.ss.technical.R
import java.lang.Exception

class ProfileActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        supportActionBar?.hide()

        var name = findViewById<TextView>(R.id.profile_name)
        var username = findViewById<TextView>(R.id.profile_username)
        var phone = findViewById<TextView>(R.id.profile_phone)
        var btn = findViewById<Button>(R.id.btn_back)

        try {
            name.text = "Name : "+Data.data.name
            username.text = "User Name : "+Data.data.user_name
            phone.text = "Phone : "+Data.data.phone
        }catch (e:Exception)
        {
            ;
        }

        btn.setOnClickListener {

            finish()

        }

    }
}