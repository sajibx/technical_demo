package com.ss.technical.Model

import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson

class User(

    var id:Int = 0,
    var name:String,
    var user_name:String,
    var password:String,
    var phone:String

)


//class User {
//
//    var id:Int = 0
//    var name:String? = null
//    var user_name:String? = null
//    var password:String? = null
//    var phone:String? = null
//
//    constructor(id:Int, name:String, user_name:String, password:String, phone:String)
//    {
//        this.id = id
//        this.name = name
//        this.user_name = user_name
//        this.password = password
//        this.phone = phone
//    }
//
//}


//{
//    class Deserializer : ResponseDeserializable<Search> {
//        override fun deserialize(content: String): Search = Gson().fromJson(content, Search::class.java)!!
//    }
//}

class Search : ArrayList<SearchItem>()
{
    class Deserializer : ResponseDeserializable<Search> {
        override fun deserialize(content: String): Search = Gson().fromJson(content, Search::class.java)!!
    }
}

data class SearchItem(
    var score: Double,
    var show: Show
)

data class Show(
    var _links: Links,
    var averageRuntime: Int,
    var dvdCountry: Any,
    var ended: Any,
    var externals: Externals,
    var genres: List<String>,
    var id: Int,
    var image: Image,
    var language: String,
    var name: String,
    var network: Network,
    var officialSite: String,
    var premiered: String,
    var rating: Rating,
    var runtime: Int,
    var schedule: Schedule,
    var status: String,
    var summary: String,
    var type: String,
    var updated: Int,
    var url: String,
    var webChannel: Any,
    var weight: Int
)

data class Links(
    var previousepisode: Previousepisode,
    var self: Self
)

data class Externals(
    var imdb: String,
    var thetvdb: Int,
    var tvrage: Any
)

data class Image(
    var medium: String,
    var original: String
)

data class Network(
    var country: Country,
    var id: Int,
    var name: String
)

data class Rating(
    var average: Double
)

data class Schedule(
    var days: List<String>,
    var time: String
)

data class Previousepisode(
    var href: String
)

data class Self(
    var href: String
)

data class Country(
    var code: String,
    var name: String,
    var timezone: String
)