package com.ss.technical.DBHelper

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ss.technical.Model.User
import java.lang.Integer.parseInt

class DBHelper(context: Context):SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VER) {

    companion object{

        private val DATABASE_NAME = "USERS.db"
        private val DATABASE_VER = 4

        private val TABLE_NAME = "USERS"
        private val COL_ID = "Id"
        private val COL_NAME = "Name"
        private val COL_USERNAME = "UserName"
        private val COL_PASSWORD = "Password"
        private val COL_PHONE = "Phone"

    }

    override fun onCreate(p0: SQLiteDatabase?) {
        val Create_Table_Querry = ("CREATE TABLE $TABLE_NAME ($COL_ID INTEGER , $COL_NAME TEXT, $COL_USERNAME TEXT, $COL_PASSWORD TEXT, $COL_PHONE TEXT)")
        p0!!.execSQL(Create_Table_Querry)
    }

    override fun onUpgrade(p0: SQLiteDatabase?, p1: Int, p2: Int) {
        p0!!.execSQL("DROP TABLE IF EXISTS $TABLE_NAME")
        onCreate(p0!!)
    }


    val allUsers:List<User>
    get() {

        val allUsers = ArrayList<User>()
        val selectQuerry = "SELECT * FROM $TABLE_NAME"
        val db = this.writableDatabase
        val courser = db.rawQuery(selectQuerry, null)
        if (courser.moveToFirst())
        {
            do {
                val user = User(courser.getColumnIndex("id")+1,"","","","")
                user.id = courser.getInt(courser.getColumnIndex(COL_ID)?:1)
                user.name = courser.getString(courser.getColumnIndex(COL_NAME)?:1)
                user.user_name = courser.getString(courser.getColumnIndex(COL_USERNAME)?:1)
                user.password = courser.getString(courser.getColumnIndex(COL_PASSWORD)?:1)
                user.phone = courser.getString(courser.getColumnIndex(COL_PHONE)?:1)

                allUsers.add(user)
            }while (courser.moveToNext())
        }

        return allUsers

    }

    fun addUser(user: User)
    {
        val db = this.writableDatabase
        val value = ContentValues()
        value.put(COL_ID, db.maximumSize+1)
        value.put(COL_NAME, user.name)
        value.put(COL_USERNAME, user.user_name)
        value.put(COL_PASSWORD, user.password)
        value.put(COL_PHONE, user.phone)

        db.insert(TABLE_NAME, null, value)
        db.close()
    }


}