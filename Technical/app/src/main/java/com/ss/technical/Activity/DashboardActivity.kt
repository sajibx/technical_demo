package com.ss.technical.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.ss.technical.Fragments.DashBoardFragment
import com.ss.technical.Model.Data
import com.ss.technical.R

class DashboardActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        supportActionBar?.hide()

        val transaction = this.supportFragmentManager!!.beginTransaction()
        var fragment = DashBoardFragment()
        transaction.replace(R.id.univarsal, fragment)
        //transaction.addToBackStack(null)
        transaction.commit()

    }
}