package com.ss.technical.Activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.ss.technical.DBHelper.DBHelper
import com.ss.technical.Model.Data
import com.ss.technical.R
import java.text.BreakIterator


class LoginActivity : AppCompatActivity() {

    internal lateinit var db: DBHelper

    var there = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        supportActionBar?.hide()

        db = DBHelper(this)

        init()

    }

    fun init()
    {
        //there = false

        var userName = findViewById<EditText>(R.id.login_userName)
        var userPass = findViewById<EditText>(R.id.login_password)
        var btnlogin = findViewById<Button>(R.id.btn_login)

        var btnreg = findViewById<TextView>(R.id.btn_reg)


        btnlogin.setOnClickListener {

            //there = false
            var list = db.allUsers

            if (!userName.text.toString().isEmpty())
            {
                if (!userPass.text.toString().isEmpty())
                {
                    list.forEach {
                         if (it.user_name==userName.text.toString())
                         {
                             if (it.password == userPass.text.toString())
                             {
                                 there = true
                                 Log.e("sajib", there.toString()+"  login")
                                 logged()
                                 Data.data = it
                             }
                         }
                        else if (it.user_name!=userName.text.toString())
                         {
                             //Toast.makeText(this, "try again", Toast.LENGTH_LONG).show()
                             there = false
                         }
                    }
                }
                else if (userName.text.toString().isEmpty())
                {
                    Toast.makeText(this, "user dosent exist", Toast.LENGTH_LONG).show()
                    there = false
                }
            }
            else if (userName.text.toString().isEmpty())
            {
                Toast.makeText(this, "user dosent exist", Toast.LENGTH_LONG).show()
                there = false
            }

            Log.e("sajib", there.toString())

//            if (there)
//            {
//                finish()
//
//                val pref = PreferenceManager.getDefaultSharedPreferences(this)
//                val editor = pref.edit()
//                editor
//                    .putString("username", userName.text.toString())
//                    .putString("password", userPass.text.toString())
//                    .apply()
//
//                startActivity(Intent(this, DashboardActivity::class.java))
//            }


        }

        btnreg.setOnClickListener {

            startActivity(Intent(this, RegisterActivity::class.java))

        }

    }

    fun logged()
    {
        var userName = findViewById<EditText>(R.id.login_userName)
        var userPass = findViewById<EditText>(R.id.login_password)

        finish()

        val pref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = pref.edit()
        editor
            .putString("username", userName.text.toString())
            .putString("password", userPass.text.toString())
            .apply()

        startActivity(Intent(this, DashboardActivity::class.java))
    }
}