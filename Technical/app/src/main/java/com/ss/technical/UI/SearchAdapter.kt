package com.ss.technical.UI

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.ss.technical.Activity.DetailActivity
import com.ss.technical.Model.Data
import com.ss.technical.Model.SearchItem
import com.ss.technical.R

class SearchAdapter(var listdata: ArrayList<SearchItem>) :RecyclerView.Adapter<SearchAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_search, parent, false)
        return SearchAdapter.ViewHolder(v)
    }

    override fun onBindViewHolder(holder: SearchAdapter.ViewHolder, position: Int) {
        var data = listdata[position]

        holder.name.text = data.show.name
        holder.genere.text = data.show.rating.toString()

        holder.layout.setOnClickListener {
            Data.index = position
            holder.itemView.context.startActivity(Intent(holder.itemView.context, DetailActivity::class.java))
        }
    }

    override fun getItemCount(): Int {
        return listdata.size
    }

    class ViewHolder(item: View):RecyclerView.ViewHolder(item)
    {
        var name = item.findViewById(R.id.full_name) as TextView
        var genere = item.findViewById(R.id.genre) as TextView
        var layout = item.findViewById(R.id.layouts) as ConstraintLayout
    }
}