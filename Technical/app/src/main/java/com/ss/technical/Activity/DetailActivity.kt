package com.ss.technical.Activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import com.ss.technical.Model.Data
import com.ss.technical.R

class DetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        supportActionBar?.hide()

        var back = findViewById<Button>(R.id.btn_back)

        var data = Data.search[Data.index]

        var name = findViewById<TextView>(R.id.full_names)
        var rating = findViewById<TextView>(R.id.rating)
        var premired = findViewById<TextView>(R.id.premired)
        var schedule = findViewById<TextView>(R.id.schedule)
        var type = findViewById<TextView>(R.id.type)
        var genre = findViewById<TextView>(R.id.genere)
        var languages = findViewById<TextView>(R.id.language)

        name.text = data.show.name
        rating.text = data.show.rating.average.toString()
        premired.text = data.show.premiered
        schedule.text = data.show.schedule.time+" on "+data.show.schedule.days
        type.text = data.show.type
        genre.text = data.show.genres.toString()
        languages.text = data.show.language

        back.setOnClickListener {

            finish()

        }

    }

}