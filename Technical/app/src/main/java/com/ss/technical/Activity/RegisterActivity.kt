package com.ss.technical.Activity

import android.content.Intent
import android.os.Bundle
import android.preference.PreferenceManager
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.ss.technical.DBHelper.DBHelper
import com.ss.technical.Model.Data
import com.ss.technical.Model.User
import com.ss.technical.R


class RegisterActivity : AppCompatActivity() {

    internal lateinit var db:DBHelper



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        supportActionBar?.hide()

        db = DBHelper(this)

        var names: EditText = findViewById(R.id.name)
        var username: EditText = findViewById(R.id.userName)
        var password: EditText = findViewById(R.id.password)
        var phone: EditText = findViewById(R.id.phone)

        var btn = findViewById<Button>(R.id.btn_register)



        btn.setOnClickListener()
        {
            if (names.text.toString().isEmpty())
            {
                Toast.makeText(this, "Name Empty", Toast.LENGTH_LONG).show()
            }
            else
            {
                if (username.text.toString().isEmpty())
                {
                    Toast.makeText(this, "User Name Empty", Toast.LENGTH_LONG).show()
                }
                else
                {
                    if (password.text.toString().isEmpty())
                    {
                        Toast.makeText(this, "Password Empty", Toast.LENGTH_LONG).show()
                    }
                    else
                    {
                        if (phone.text.toString().isEmpty())
                        {
                            Toast.makeText(this, "Phone Empty", Toast.LENGTH_LONG).show()
                        }
                        else
                        {
                            //Toast.makeText(this, "No Problem", Toast.LENGTH_LONG).show()
                            //db.addUser(User(0,names.text.toString(),username.text.toString(),password.text.toString(),phone.text.toString()))
                            //Log.e("sajib", "----"+db.allUsers.size.toString())
                            if (db.allUsers.isEmpty())
                            {
                                db.addUser(User(0,names.text.toString(),username.text.toString(),password.text.toString(),phone.text.toString()))
                                Data.data = (User(0,names.text.toString(),username.text.toString(),password.text.toString(),phone.text.toString()))
                                //Toast.makeText(this, "Save User 1", Toast.LENGTH_LONG).show()
                                finish()
                                Log.e("sajib", "---->"+db.allUsers.size.toString())

                                val pref = PreferenceManager.getDefaultSharedPreferences(this)
                                val editor = pref.edit()
                                editor
                                    .putString("username", username.text.toString())
                                    .putString("password", password.text.toString())
                                    .apply()

                                startActivity(Intent(this, DashboardActivity::class.java))
                            }
                            else
                            {
                                var list = ArrayList<User>()
                                db.allUsers.forEach {
                                    if (username.text.toString()==it.user_name)
                                    {
                                        list.add(it)
                                    }
                                }

                                if (list.isEmpty())
                                {
                                    db.addUser(User(0,names.text.toString(),username.text.toString(),password.text.toString(),phone.text.toString()))
                                    Data.data = (User(0,names.text.toString(),username.text.toString(),password.text.toString(),phone.text.toString()))
                                    //Toast.makeText(this, "Save User 2", Toast.LENGTH_LONG).show()
                                    finish()
                                    Log.e("sajib", "---->"+db.allUsers.size.toString())

                                    val pref = PreferenceManager.getDefaultSharedPreferences(this)
                                    val editor = pref.edit()
                                    editor
                                        .putString("username", username.text.toString())
                                        .putString("password", password.text.toString())
                                        .apply()

                                    startActivity(Intent(this, DashboardActivity::class.java))
                                }
                                else
                                {
                                    Toast.makeText(this, "User Exist", Toast.LENGTH_LONG).show()
                                }
                            }
                        }
                    }
                }
            }
        }

        //Log.e("sajib", "----"+db.allUsers.size.toString())
    }
}